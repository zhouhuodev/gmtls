# gmtls

[基于同济区块链研究院有限公司的gmtls](https://github.com/tjfoc/gmtls.git)

GM TLS/SSL Based on Golang (基于国密算法的TLS/SSL代码库)

版权所有 苏州同济区块链研究院有限公司(http://www.wutongchain.com)

兼容 google.golang.org/grpc@v1.29.1
