module gitee.com/zhouhuodev/gmtls

go 1.14

require (
	gitee.com/zhouhuodev/gmsm v0.0.0-20200624092415-71cb6201d680
	github.com/golang/protobuf v1.4.2
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd
	google.golang.org/grpc v1.29.1
)
